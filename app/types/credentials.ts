export interface Credentials {
  accessToken: string;
  uid: string;
  client: string;
  userId: string;
}
