export type ItemType =
  | 'projects'
  | 'project'
  | 'communities'
  | 'challenges'
  | 'programs'
  | 'posts'
  | 'needs'
  | 'users'
  | 'people';
