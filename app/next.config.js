/* eslint-disable no-param-reassign */
const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
});
const withTM = require('next-transpile-modules')([
  '@formatjs/intl-relativetimeformat',
  '@formatjs/intl-utils',
  'react-intl',
  'intl-format-cache',
  'intl-messageformat-parser',
  'intl-messageformat',
]);

const nextConfig = {
  typescript: {
    // !! WARN !!
    // Dangerously allow production builds to successfully complete even if
    // your project has type errors.
    // !! WARN !!
    ignoreBuildErrors: true,
  },
  // manage env variables
  env: {
    ALGOLIA_APP_ID: process.env.ALGOLIA_APP_ID,
    ALGOLIA_TOKEN: process.env.ALGOLIA_TOKEN,
    ADDRESS_BACK: process.env.ADDRESS_BACK,
    ADDRESS_FRONT: process.env.ADDRESS_FRONT,
    GOOGLE_ANALYTICS_ID: process.env.GOOGLE_ANALYTICS_ID,
    TAGMANAGER_ARGS_ID: process.env.TAGMANAGER_ARGS_ID,
    HOTJAR_ID: process.env.HOTJAR_ID,
  },
  // manage redirection
  redirects() {
    return [
      { source: '/projects', destination: '/search/projects', permanent: true },
      { source: '/needs', destination: '/search/needs', permanent: true },
      { source: '/people', destination: '/search/members', permanent: true },
      { source: '/communities', destination: '/search/groups', permanent: true },
      { source: '/challenges', destination: '/search/challenges', permanent: true },
    ];
  },
};

module.exports = withTM(withBundleAnalyzer(nextConfig));
