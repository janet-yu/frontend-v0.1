import Select from 'react-select';
import { useState } from 'react';
import { useApi } from '~/contexts/apiContext';
import { useIntl } from 'react-intl';

const DropdownRole = ({
  actualRole = 'member',
  callBack = () => {
    console.warn('Missing callback function');
  },
  onRoleChanged = () => {
    console.warn('Missing function');
  },
  itemId = undefined,
  itemType = undefined,
  listRole = [],
  member = undefined,
  isDisabled = false,
}) => {
  const [sending, setSending] = useState(false);
  const api = useApi();
  const { formatMessage } = useIntl();

  const handleChange = (key) => {
    if (itemId || itemType || member.id) {
      const jsonToSend = {
        user_id: member.id,
        previous_role: actualRole,
        new_role: key.value.toLowerCase(),
      };
      setSending(true);
      api
        .post(`/api/${itemType}/${itemId}/members`, jsonToSend)
        .then(() => {
          setSending(false);
          callBack();
          onRoleChanged();
        })
        .catch((err) => {
          console.error(`Couldn't POST ${itemType} with itemId=${itemId} members`, err);
          setSending(false);
        });
    }
  };

  return (
    <>
      <Select
        options={listRole.map((role) => ({
          value: role,
          label: formatMessage({ id: `member.role.${role}`, defaultMessage: role }),
        }))}
        isSearchable={false}
        isDisabled={!isDisabled || sending}
        menuShouldScrollIntoView={true} // force scroll into view
        defaultValue={{
          value: actualRole,
          label: formatMessage({ id: `member.role.${actualRole}`, defaultMessage: actualRole }),
        }}
        onChange={handleChange}
      />
      {sending && (
        <>
          <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
          &nbsp;
        </>
      )}
    </>
  );
};
export default DropdownRole;
