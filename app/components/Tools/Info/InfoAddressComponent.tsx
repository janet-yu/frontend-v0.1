import { Component } from 'react';
import TitleInfo from '~/components/Tools/TitleInfo';
// import "./InfoAddressComponent.scss";

export default class InfoAddressComponent extends Component {
  static get defaultProps() {
    return {
      title: 'Title',
      address: '',
      city: '',
      country: '',
    };
  }

  render() {
    const { title, address, city, country } = this.props;
    let titleinfo = '';

    if (title) {
      titleinfo = <TitleInfo title={title} />;
    }

    if (address || city || country) {
      return (
        <div className="infoAddress">
          {titleinfo}
          <div className="content">
            {/* <p id="address">{address}</p>
            <p id="city">{city}</p>
            <p id="country">{country}</p> */}
            <span id="full_address">
              {/* {address ? address+", " : ""} */}
              {city && country ? `${city}, ` : city && city}
              {country}
            </span>
          </div>
        </div>
      );
    }
    // eslint-disable-next-line @rushstack/no-null
    return null;
  }
}
