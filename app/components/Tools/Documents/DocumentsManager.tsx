import { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import BtnUploadFile from '~/components/Tools/BtnUploadFile';
import DocumentsList from '~/components/Tools/Documents/DocumentsList';
import Loading from '~/components/Tools/Loading';
import { ApiContext } from '~/contexts/apiContext';
import { ItemType } from '~/types';
import Box from '~/components/Box';

interface Props {
  itemId: number;
  itemType: ItemType;
  // TODO: Use the correct type, not any;
  item: any;
  showTitle?: boolean;
}
interface State {
  item: any;
  loading: boolean;
}
export default class DocumentsManager extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      item: this.props.item,
      loading: true,
    };
  }

  UNSAFE_componentWillMount() {
    if (this.props.item) {
      this.setState({ loading: false });
    } else {
      this.getItemApi();
    }
  }

  componentDidMount() {
    this.getItemApi();
  }

  refresh() {
    this.getItemApi();
  }

  getItemApi() {
    const { itemId, itemType } = this.props;
    if (!itemId || !itemType) {
      console.warn('parameters is missing');
    } else {
      const api = this.context;
      this.setState({ loading: true });
      api
        .get(`/api/${itemType}/${itemId}`)
        .then((res) => {
          if (res.data) {
            this.setState({ item: res.data, loading: false });
          } else {
            this.setState({ loading: false });
          }
        })
        .catch((err) => {
          console.error(`Couldn't GET ${itemType} with itemId=${itemId}`, err);
          this.setState({ loading: false });
        });
    }
  }

  render() {
    const { item, loading } = this.state;
    const { itemId, itemType, showTitle } = this.props;
    return (
      <Loading active={loading}>
        {item && (
          <>
            {item.documents.length !== 0 && showTitle && (
              <Box fontWeight="bold">
                <FormattedMessage id="entity.tab.documents" defaultMessage="Documents" />
              </Box>
            )}
            <div className="documentsManager">
              {item.is_admin && (
                <BtnUploadFile
                  itemId={itemId}
                  itemType={itemType}
                  multiple
                  refresh={this.refresh.bind(this)}
                  text={<FormattedMessage id="info-1003" defaultMessage="Choose a file" />}
                  type="documents"
                  uploadNow
                />
              )}
              <DocumentsList
                documents={item.documents}
                cardType="cards"
                item={item}
                itemType={itemType}
                refresh={this.refresh.bind(this)}
              />
            </div>
          </>
        )}
      </Loading>
    );
  }
}
DocumentsManager.contextType = ApiContext;
