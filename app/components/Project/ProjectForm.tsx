import { Component } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import Link from 'next/link';
/** * Form objects ** */
import FormDefaultComponent from '~/components/Tools/Forms/FormDefaultComponent';
import FormTextAreaComponent from '~/components/Tools/Forms/FormTextAreaComponent';
import FormImgComponent from '~/components/Tools/Forms/FormImgComponent';
import FormInterestsComponent from '~/components/Tools/Forms/FormInterestsComponent';
import FormSkillsComponent from '~/components/Tools/Forms/FormSkillsComponent';
import FormToggleComponent from '~/components/Tools/Forms/FormToggleComponent';
import FormWysiwygComponent from '~/components/Tools/Forms/FormWysiwygComponent';
/** * Validators ** */
import FormValidator from '~/components/Tools/Forms/FormValidator';
import projectFormRules from './projectFormRules';
import { toAlphaNum } from '~/components/Tools/Nickname';
/** * Images/Style ** */

class ProjectForm extends Component {
  validator = new FormValidator(projectFormRules);

  static get defaultProps() {
    return {
      mode: 'create',
      project: {
        banner_url: '',
        description: '',
        interests: [],
        followers: 0,
        members: 0,
        short_description: '',
        short_title: '',
        skills: [],
        title: '',
        status: '',
        is_private: false,
      },
      sending: false,
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      isChecked: false,
    };
  }

  handleToggleChange = () => {
    if (this.state.isChecked) {
      document.querySelector('.detailed-description').style.display = 'none';
      document.querySelector('.basic-description').style.display = 'block';
    } else {
      document.querySelector('.detailed-description').style.display = 'block';
      document.querySelector('.basic-description').style.display = 'none';
    }
    this.setState({
      isChecked: !this.state.isChecked,
    });
  };

  generateSlug(projectTitle) {
    let proposalShortName = projectTitle.trim();
    proposalShortName = toAlphaNum(proposalShortName);
    return proposalShortName;
  }

  handleChange(key, content) {
    let proposalShortName;
    if (key === 'title') {
      // generate a shortname when typing a title, and update short_name field with the value
      proposalShortName = this.generateSlug(content);
      this.props.handleChange('short_title', proposalShortName);
    }
    /* Validators start */
    const state = {};
    state[key] = content;
    // Check proposalShortName
    if (key === 'title') {
      state.short_title = proposalShortName;
    }
    const validation = this.validator.validate(state);
    if (validation[key] !== undefined) {
      const stateValidation = {};
      stateValidation[`valid_${key}`] = validation[key];
      // Update short_title too only if short_title has been changed
      if (key === 'title') {
        stateValidation.valid_short_title = validation.short_title;
      }
      this.setState(stateValidation);
    }
    /* Validators end */
    this.props.handleChange(key, content);
  }

  handleSubmit() {
    /* Validators control before submit */
    let firsterror = true;
    const validation = this.validator.validate(this.props.project);
    if (validation.isValid) {
      this.props.handleSubmit();
    } else {
      const stateValidation = {};
      Object.keys(validation).forEach((key) => {
        if (key !== 'isValid') {
          if (validation[key].isInvalid && firsterror) {
            // if field is invalid and it's the first field that has error
            const element = document.querySelector(`#${key}`); // get element that is not valid
            const y = element.getBoundingClientRect().top + window.pageYOffset - 140; // calculate it's top value and remove 25 of offset
            window.scrollTo({ top: y, behavior: 'smooth' }); // scroll to element to show error
            firsterror = false; // set to false so that it won't scroll to second invalid field and further
          }
          stateValidation[`valid_${key}`] = validation[key];
        }
      });
      this.setState(stateValidation);
    }
  }

  renderBtnsForm() {
    const { mode, project, sending } = this.props;
    let urlBack = '/search/[active-index]';
    let urlBackAs = '/search/projects';
    let textActionId = 'completeProfile.btn.next';
    let textAction = 'Next';
    if (mode === 'edit') {
      urlBackAs = `/project/${project.id}/${project.short_title}`;
      urlBack = '/project/[id]/[[...index]]';
      textActionId = 'entity.form.btnUpdate';
      textAction = 'Update';
    }

    return (
      <div className="row projectFormBtns">
        <Link href={urlBack} as={urlBackAs}>
          <a>
            <button type="button" className="btn btn-outline-primary" disabled={sending}>
              <FormattedMessage id="entity.form.btnCancel" defaultMessage="Cancel" />
            </button>
          </a>
        </Link>
        <button
          type="button"
          onClick={this.handleSubmit.bind(this)}
          className="btn btn-primary"
          disabled={sending}
          style={{ marginRight: '10px' }}
        >
          {sending && (
            <>
              <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
              &nbsp;
            </>
          )}
          <FormattedMessage id={textActionId} defaultMessage={textAction} />
        </button>
      </div>
    );
  }

  render() {
    const { valid_title, valid_short_title, valid_short_description, valid_interests, valid_skills } = this.state
      ? this.state
      : '';
    const { project, mode, intl } = this.props;

    if (
      project.desc_elevator_pitch ||
      project.desc_problem_statement ||
      project.desc_objectives ||
      project.desc_state_art ||
      project.desc_progress ||
      project.desc_stakeholder ||
      project.desc_impact_strat ||
      project.desc_ethical_statement ||
      project.desc_sustainability_scalability ||
      project.desc_communication_strat ||
      project.desc_funding ||
      project.desc_contributing
    ) {
      var old_description = true;
    } else {
      var old_description = false;
    }

    return (
      <form className="projectForm">
        <FormDefaultComponent
          content={project.title}
          errorCodeMessage={valid_title ? valid_title.message : ''}
          id="title"
          isValid={valid_title ? !valid_title.isInvalid : undefined}
          mandatory
          onChange={this.handleChange.bind(this)}
          placeholder={intl.formatMessage({
            id: 'project.form.title.placeholder',
            defaultMessage: 'An awesome project',
          })}
          title={intl.formatMessage({ id: 'entity.info.title', defaultMessage: 'Title' })}
        />
        <FormDefaultComponent
          content={project.short_title}
          errorCodeMessage={valid_short_title ? valid_short_title.message : ''}
          id="short_title"
          isValid={valid_short_title ? !valid_short_title.isInvalid : undefined}
          onChange={this.handleChange.bind(this)}
          mandatory
          pattern={/[A-Za-z0-9]/g}
          placeholder={intl.formatMessage({
            id: 'project.form.short_title.placeholder',
            defaultMessage: 'AnAwesomeProject',
          })}
          prepend="#"
          title={intl.formatMessage({ id: 'entity.info.short_name', defaultMessage: 'Short Name' })}
        />
        <FormTextAreaComponent
          content={project.short_description}
          errorCodeMessage={valid_short_description ? valid_short_description.message : ''}
          id="short_description"
          isValid={valid_short_description ? !valid_short_description.isInvalid : undefined}
          mandatory
          maxChar={240}
          onChange={this.handleChange.bind(this)}
          placeholder={intl.formatMessage({
            id: 'project.form.short_description.placeholder',
            defaultMessage: 'The project briefly explained',
          })}
          rows={5}
          title={intl.formatMessage({ id: 'entity.info.short_description', defaultMessage: 'Short description' })}
        />
        {mode === 'edit' && (
          <>
            <FormWysiwygComponent
              content={project.description}
              id="description"
              onChange={this.handleChange.bind(this)}
              show
              placeholder={intl.formatMessage({
                id: 'project.form.description.placeholder',
                defaultMessage: 'Describe your project in detail, with formatted text, images...',
              })}
              title={intl.formatMessage({ id: 'entity.info.description', defaultMessage: 'Description' })}
            />
            {old_description && (
              <a
                className="btn btn-primary collapseDetailedInfo"
                data-toggle="collapse"
                href="#DetailedAbout"
                role="button"
                aria-expanded="false"
              >
                <FormattedMessage id="project.fillDetailedInfo" defaultMessage="Edit" />
              </a>
            )}
            <div className="collapse" id="DetailedAbout">
              <FormWysiwygComponent
                content={project.desc_elevator_pitch}
                id="desc_elevator_pitch"
                onChange={this.handleChange.bind(this)}
                show
                placeholder={intl.formatMessage({
                  id: 'project.form.desc_elevator_pitch.placeholder',
                  defaultMessage:
                    'Describe briefly your project in non-technical language: the issue you’re addressing, the proposed solution and the expected results. (approx 200 words)',
                })}
                title={intl.formatMessage({
                  id: 'entity.info.desc_elevator_pitch',
                  defaultMessage: 'Elevator pitch / Abstract',
                })}
              />
              <FormWysiwygComponent
                content={project.desc_contributing}
                id="desc_contributing"
                onChange={this.handleChange.bind(this)}
                show
                placeholder={intl.formatMessage({
                  id: 'project.form.desc_contributing.placeholder',
                  defaultMessage:
                    'Describe how people can join your project, instructions for collaboration, tools and communication methods, code of conduct to follow. (approx 100 words)',
                })}
                title={intl.formatMessage({ id: 'entity.info.desc_contributing', defaultMessage: 'How to contribute' })}
              />
              <FormWysiwygComponent
                content={project.desc_problem_statement}
                id="desc_problem_statement"
                onChange={this.handleChange.bind(this)}
                show
                placeholder={intl.formatMessage({
                  id: 'project.form.desc_problem_statement.placeholder',
                  defaultMessage: 'Provide details of the problem you propose to solve (approx 200 words)',
                })}
                title={intl.formatMessage({
                  id: 'entity.info.desc_problem_statement',
                  defaultMessage: 'Problem Statement',
                })}
              />
              <FormWysiwygComponent
                content={project.desc_objectives}
                id="desc_objectives"
                onChange={this.handleChange.bind(this)}
                show
                placeholder={intl.formatMessage({
                  id: 'project.form.desc_objectives.placeholder',
                  defaultMessage: 'Describe how your project addresses the identified problem (approx 200 words)',
                })}
                title={intl.formatMessage({
                  id: 'entity.info.desc_objectives',
                  defaultMessage: 'Objectives & Methodology',
                })}
              />
              <FormWysiwygComponent
                content={project.desc_state_art}
                id="desc_state_art"
                onChange={this.handleChange.bind(this)}
                show
                placeholder={intl.formatMessage({
                  id: 'project.form.desc_state_art.placeholder',
                  defaultMessage: 'How does your project differ from existing approaches? (approx 150 words)',
                })}
                title={intl.formatMessage({ id: 'entity.info.desc_state_art', defaultMessage: 'State of the art' })}
              />
              <FormWysiwygComponent
                content={project.desc_progress}
                id="desc_progress"
                onChange={this.handleChange.bind(this)}
                show
                placeholder={intl.formatMessage({
                  id: 'project.form.desc_progress.placeholder',
                  defaultMessage:
                    'Describe the state of progress towards your set goal and the timeline you plan to follow for the project development. Use the Need object on JOGL accordingly. (approx 250 words + 5 need object max)',
                })}
                title={intl.formatMessage({ id: 'entity.info.desc_progress', defaultMessage: 'Progress report' })}
              />
              <FormWysiwygComponent
                content={project.desc_stakeholder}
                id="desc_stakeholder"
                onChange={this.handleChange.bind(this)}
                show
                placeholder={intl.formatMessage({
                  id: 'project.form.desc_stakeholder.placeholder',
                  defaultMessage:
                    'Describe how your project engages and aligns with stakeholders (or plans to do so) (approx 150 words)',
                })}
                title={intl.formatMessage({ id: 'entity.info.desc_stakeholder', defaultMessage: 'Stakeholders' })}
              />
              <FormWysiwygComponent
                content={project.desc_impact_strat}
                id="desc_impact_strat"
                onChange={this.handleChange.bind(this)}
                show
                placeholder={intl.formatMessage({
                  id: 'project.form.desc_impact_strat.placeholder',
                  defaultMessage:
                    'Describe the strategy to implement, monitor and evaluate your project.. What are the criteria you use to measure the impact of your project? What are the expected short- and long-term impact? (approx 250 words)',
                })}
                title={intl.formatMessage({ id: 'entity.info.desc_impact_strat', defaultMessage: 'Impact strategy' })}
              />
              <FormWysiwygComponent
                content={project.desc_ethical_statement}
                id="desc_ethical_statement"
                onChange={this.handleChange.bind(this)}
                show
                placeholder={intl.formatMessage({
                  id: 'project.form.desc_ethical_statement.placeholder',
                  defaultMessage:
                    'Describe how your project takes into consideration  ecological, environmental and social (approx 200 words)',
                })}
                title={intl.formatMessage({
                  id: 'entity.info.desc_ethical_statement',
                  defaultMessage: 'Ethical considerations',
                })}
              />
              <FormWysiwygComponent
                content={project.desc_sustainability_scalability}
                id="desc_sustainability_scalability"
                onChange={this.handleChange.bind(this)}
                show
                placeholder={intl.formatMessage({
                  id: 'project.form.desc_sustainability_scalability.placeholder',
                  defaultMessage:
                    'How will your project sustain its development in the next few months/years? How can it broaden its scope and impact? (approx 100 words)',
                })}
                title={intl.formatMessage({
                  id: 'entity.info.desc_sustainability_scalability',
                  defaultMessage: 'Sustainability and scalability',
                })}
              />
              <FormWysiwygComponent
                content={project.desc_communication_strat}
                id="desc_communication_strat"
                onChange={this.handleChange.bind(this)}
                show
                placeholder={intl.formatMessage({
                  id: 'project.form.desc_communication_strat.placeholder',
                  defaultMessage:
                    'Describe what steps you are taking to communicate about your project and disseminate the results (approx 100 words)',
                })}
                title={intl.formatMessage({
                  id: 'entity.info.desc_communication_strat',
                  defaultMessage: 'Communication and dissemination strategy',
                })}
              />
              <FormWysiwygComponent
                content={project.desc_funding}
                id="desc_funding"
                onChange={this.handleChange.bind(this)}
                show
                placeholder={intl.formatMessage({
                  id: 'project.form.desc_funding.placeholder',
                  defaultMessage: 'Is your project funded, or soon to be? If yes, how? (approx 50 words)',
                })}
                title={intl.formatMessage({ id: 'entity.info.desc_funding', defaultMessage: 'Funding' })}
              />
            </div>
          </>
        )}
        {mode === 'edit' && (
          <FormImgComponent
            type="banner"
            id="banner_url"
            imageUrl={project.banner_url}
            itemId={project.id}
            itemType="projects"
            title={intl.formatMessage({ id: 'project.info.banner_url', defaultMessage: 'Project banner' })}
            content={project.banner_url}
            defaultImg="/images/default/default-project.jpg"
            onChange={this.handleChange.bind(this)}
          />
        )}
        <FormInterestsComponent
          content={project.interests}
          errorCodeMessage={valid_interests ? valid_interests.message : ''}
          id="interests"
          isValid={valid_interests ? !valid_interests.isInvalid : undefined}
          mandatory
          onChange={this.handleChange.bind(this)}
          title={intl.formatMessage({ id: 'entity.info.interests', defaultMessage: 'Interests' })}
        />
        <FormSkillsComponent
          content={project.skills}
          errorCodeMessage={valid_skills ? valid_skills.message : ''}
          id="skills"
          type="project"
          isValid={valid_skills ? !valid_skills.isInvalid : undefined}
          mandatory
          onChange={this.handleChange.bind(this)}
          placeholder={intl.formatMessage({
            id: 'general.skills.placeholder',
            defaultMessage: 'Big data, Web Development, Open Science...',
          })}
          title={intl.formatMessage({ id: 'entity.info.skills', defaultMessage: 'Skills' })}
        />
        {/* {mode === "edit" &&
          <FormDropdownComponent
            id="status"
            title={intl.formatMessage({id:'entity.info.status' })}
            content={project.status}
            // options={["active", "archived", "draft", "completed"]}
            options={["draft", "soon", "active", "completed"]}
            onChange={this.handleChange.bind(this)} />
        } */}
        {mode === 'edit' && (
          <FormToggleComponent
            id="is_private"
            warningMsg="By choosing 'public', any member can join your project without your approval."
            warningMsgId="project.info.publicPrivateToggleMsg"
            title={intl.formatMessage({ id: 'entity.info.is_private', defaultMessage: 'Confidentiality' })}
            choice1={<FormattedMessage id="general.public" defaultMessage="Public" />}
            choice2={<FormattedMessage id="general.private" defaultMessage="Private" />}
            color1="#F9530B"
            color2="#27B40E"
            isChecked={project.is_private}
            onChange={this.handleChange.bind(this)}
          />
        )}
        {this.renderBtnsForm()}
      </form>
    );
  }
}

export default injectIntl(ProjectForm);
