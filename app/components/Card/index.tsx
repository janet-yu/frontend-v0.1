import Link from 'next/link';
import React, { FC, ReactNode } from 'react';
import { SpaceProps } from 'styled-system';
import styled from '~/utils/styled';
import { useTheme } from '~/utils/theme';
import Box, { BoxProps } from '../Box';

interface IContainer {
  noMobileBorder: boolean;
}
const Container = styled(Box)<IContainer>`
  box-shadow: ${(p) => p.theme.shadows.default};
  border-radius: ${(p) => p.theme.radii.xl};
  border: 1px solid ${(p) => p.theme.colors.greys['200']};
  background: white;
  @media (max-width: ${(p) => p.theme.breakpoints.sm}) {
    // ! Do not use !important
    box-shadow: ${(p) => (p.noMobileBorder ? 'none!important' : p.theme.shadows.default)};
    border: ${(p) => (p.noMobileBorder ? 'none!important' : p.theme.shadows.default)};
    border-bottom: ${(p) => (p.noMobileBorder ? `1px solid lightgrey!important` : 'none!important')};
    border-radius: ${(p) => (p.noMobileBorder ? '0px!important' : p.theme.radii.xl)};
  }
`;

interface BackgroundImgProps {
  small: boolean;
  url: string;
}
const BackgroundImg = styled.div<BackgroundImgProps>`
  background-image: url("${(p) => p.url}");
  background-size: cover;
  background-position: center center;
  background-repeat: no-repeat;
  border-top-left-radius:${(p) => p.theme.radii.xl};
  border-top-right-radius:${(p) => p.theme.radii.xl};
  width: 100%;
  height: ${(p) => (p.small ? p.theme.sizes[13] : p.theme.sizes[14])};
  @media (max-width: ${(p) => p.theme.breakpoints.sm}) {
    height: ${(p) => (p.small ? p.theme.sizes[12] : p.theme.sizes[13])};
  }
  &:hover {
    opacity:.9;
  }
`;
interface CardProps extends BoxProps {
  imgUrl?: string;
  imgLinkObject?: {
    as?: string;
    href: string;
  };
  isImgSmall?: boolean;
  children: ReactNode;
  noMobileBorder?: boolean;
  containerStyle?: any;
  spaceY?: SpaceProps['marginTop'];
}

const Card: FC<CardProps> = ({
  imgUrl,
  children,
  imgLinkObject,
  isImgSmall,
  spaceY = [3, 4],
  noMobileBorder,
  ...props
}) => {
  const theme = useTheme();
  return (
    <Container noMobileBorder={noMobileBorder} {...props.containerStyle}>
      {imgUrl &&
      imgLinkObject && ( // if card has image and image is also a link
          <Link href={imgLinkObject.href} as={imgLinkObject.as}>
            <a>
              <BackgroundImg url={imgUrl} small={isImgSmall} />
            </a>
          </Link>
        )}
      {
        imgUrl && !imgLinkObject && (
          <BackgroundImg url={imgUrl} small={isImgSmall} />
        ) /* if card has image and no img link*/
      }
      <Box
        py={4}
        px={noMobileBorder ? [0, 4] : 4}
        flex="1"
        spaceY={spaceY}
        borderTop={imgUrl && `1px solid ${theme.colors.greys['300']}`}
        {...props}
      >
        {children}
      </Box>
    </Container>
  );
};
export default Card;
