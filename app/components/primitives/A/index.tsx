import Link, { LinkProps } from 'next/link';
import React, { FC } from 'react';
import styled from '~/utils/styled';
import ReactGA from 'react-ga';

interface IContainer {
  noStyle: boolean;
}
const Container = styled.a<IContainer>`
  font-size: ${(p) => p.theme.fontSizes[2]};
  color: ${(p) => (!p.noStyle ? p.theme.colors.primary : 'inherit')};
  cursor: pointer;
  :hover {
    text-decoration: ${(p) => (!p.noStyle ? 'underline' : 'none')} !important;
  }
`;
interface Props extends LinkProps {
  noStyle?: boolean;
}
const recordClickEvent = (route) => {
  // send click event to google analytics
  ReactGA.event({ category: 'Link', action: 'click', label: route });
};
const A: FC<Props> = ({ noStyle, children, ...props }) => {
  return (
    <Link {...props} passHref>
      <Container noStyle={noStyle} onClick={() => recordClickEvent(props.as)}>
        {children}
      </Container>
    </Link>
  );
};
export default A;
