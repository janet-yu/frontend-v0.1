import { useRouter } from 'next/router';
import React, { useContext, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import Box from '~/components/Box';
import Button from '~/components/primitives/Button';
import P from '~/components/primitives/P';
import Alert from '~/components/Tools/Alert';
import { useApi } from '~/contexts/apiContext';
import { useModal } from '~/contexts/modalContext';
import { UserContext } from '~/contexts/UserProvider';
import Translate from '~/utils/Translate';
import ReactGA from 'react-ga';

const UserDelete = ({ userId }) => {
  const [errors, setErrors] = useState();
  const { showModal, setIsOpen } = useModal();
  const api = useApi();
  const router = useRouter();
  const userContext = useContext(UserContext);

  const deleteAccount = (event) => {
    event.preventDefault();
    const headers = {
      'access-token': userContext.credentials.accessToken,
      client: userContext.credentials.client,
      uid: userContext.credentials.uid,
    };
    api
      .delete(`/api/users`, { headers })
      .then(() => {
        ReactGA.event({ category: 'User', action: 'delete', label: `user ${userId}` }); // record event to Google Analytics
        userContext.logout(); // logout user
        setIsOpen(false); // close modal
        router.push('/', `/`); // redirect to homepage
      })
      .catch((error) => {
        setErrors(error.toString()); // show errors
      });
  };

  const errorMessage = errors?.includes('err-') ? (
    <FormattedMessage id={errors} defaultMessage="An error has occurred" />
  ) : (
    errors
  );
  return (
    <Button
      onClick={() => {
        showModal({
          children: (
            <>
              {errors && <Alert type="danger" message={errorMessage} />}
              <P fontWeight="600" fonSize="1rem">
                <FormattedMessage
                  id="settings.account.delete.modal.message"
                  defaultMessage="Are you sure to delete your JOGL account ?*"
                />
              </P>
              <P fontSize=".9rem">
                <FormattedMessage
                  id="settings.account.delete.modal.info"
                  defaultMessage="(* : The account will be archived for 30 days before a permanent deletion)"
                />
              </P>
              <Box row spaceX={3}>
                <Button btnType="danger" onClick={deleteAccount}>
                  <FormattedMessage id="general.yes" defaultMessage="Yes" />
                </Button>
                <Button onClick={() => setIsOpen(false)}>
                  <FormattedMessage id="general.no" defaultMessage="No" />
                </Button>
              </Box>
            </>
          ),
          title: 'Delete my JOGL account',
          titleId: 'settings.account.delete.btn',
          maxWidth: '30rem',
        });
      }}
      btnType="danger"
    >
      <Translate id="settings.account.delete.btn" defaultMessage="Delete my JOGL account" />
    </Button>
  );
};
export default UserDelete;
