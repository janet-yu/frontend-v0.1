/* eslint-disable camelcase */
import { Component } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import Link from 'next/link';
// import MembersList from "~/components/Members/MembersList";
/** * Form objects ** */
import FormDefaultComponent from '~/components/Tools/Forms/FormDefaultComponent';
import FormImgComponent from '~/components/Tools/Forms/FormImgComponent';
import FormInterestsComponent from '~/components/Tools/Forms/FormInterestsComponent';
import FormSkillsComponent from '~/components/Tools/Forms/FormSkillsComponent';
import FormResourcesComponent from '~/components/Tools/Forms/FormResourcesComponent';
import FormTextAreaComponent from '~/components/Tools/Forms/FormTextAreaComponent';
import FormToggleComponent from '~/components/Tools/Forms/FormToggleComponent';
import FormWysiwygComponent from '~/components/Tools/Forms/FormWysiwygComponent';
/** * Validators ** */
import FormValidator from '~/components/Tools/Forms/FormValidator';
import communityFormRules from './communityFormRules';
import { toAlphaNum } from '~/components/Tools/Nickname';
/** * Images/Style ** */
// import "./CommunityForm.scss";

class CommunityForm extends Component {
  validator = new FormValidator(communityFormRules);

  static get defaultProps() {
    return {
      mode: 'create',
      community: {
        banner_url: '',
        description: '',
        interests: [],
        short_description: '',
        short_title: '',
        skills: [],
        resources: [],
        title: '',
        is_private: false,
      },
      sending: false,
    };
  }

  generateSlug(grouTitle) {
    let proposalShortName = grouTitle.trim();
    proposalShortName = toAlphaNum(proposalShortName);
    return proposalShortName;
  }

  handleChange(key, content) {
    let proposalShortName;
    if (key === 'title') {
      // generate a shortname when typing a title, and update short_name field with the value
      proposalShortName = this.generateSlug(content);
      this.props.handleChange('short_title', proposalShortName);
    }
    /* Validators start */
    const state = {};
    state[key] = content;
    // Check proposalShortName
    if (key === 'title') {
      state.short_title = proposalShortName;
    }
    const validation = this.validator.validate(state);
    if (validation[key] !== undefined) {
      const stateValidation = {};
      stateValidation[`valid_${key}`] = validation[key];
      // Update short_title too only if short_title has been changed
      if (key === 'title') {
        stateValidation.valid_short_title = validation.short_title;
      }
      this.setState(stateValidation);
    }
    /* Validators end */
    this.props.handleChange(key, content);
  }

  handleSubmit() {
    /* Validators control before submit */
    let firstError = true;
    const validation = this.validator.validate(this.props.community);
    if (validation.isValid) {
      this.props.handleSubmit();
    } else {
      const stateValidation = {};
      Object.keys(validation).forEach((key) => {
        if (key !== 'isValid') {
          if (validation[key].isInvalid && firstError) {
            // if field is invalid and it's the first field that has error
            const element = document.querySelector(`#${key}`); // get element that is not valid
            const y = element.getBoundingClientRect().top + window.pageYOffset - 140; // calculate it's top value and remove 25 of offset
            window.scrollTo({ top: y, behavior: 'smooth' }); // scroll to element to show error
            firstError = false; // set to false so that it won't scroll to second invalid field and further
          }
          stateValidation[`valid_${key}`] = validation[key];
        }
      });
      this.setState(stateValidation);
    }
  }

  disableBtn() {
    const { title, short_title } = this.props.community;
    if (title.trim() === '' || short_title.trim() === '') {
      return true;
    }
    return false;
  }

  renderBtnsForm() {
    const { community, mode, sending } = this.props;
    let urlBack = '/search/[active-index]';
    let urlBackAs = '/search/groups';
    let textAction = 'Create';
    if (mode === 'edit') {
      urlBack = '/community/[id]/[[...index]]';
      urlBackAs = `/community/${community.id}/${community.short_title}`;
      textAction = 'Update';
    }

    return (
      <div className="row communityFormBtns">
        <Link href={urlBack} as={urlBackAs}>
          <a>
            <button type="button" className="btn btn-outline-primary">
              <FormattedMessage id="entity.form.btnCancel" defaultMessage="Cancel" />
            </button>
          </a>
        </Link>
        <button
          type="button"
          className="btn btn-primary"
          onClick={this.handleSubmit.bind(this)}
          style={{ marginRight: '10px' }}
          disabled={this.disableBtn() || sending}
        >
          {sending && (
            <>
              <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
              &nbsp;
            </>
          )}
          <FormattedMessage id={`entity.form.btn${textAction}`} defaultMessage={textAction} />
        </button>
      </div>
    );
  }

  render() {
    const { valid_title, valid_short_title, valid_short_description, valid_interests, valid_skills } = this.state
      ? this.state
      : '';
    const { community, mode, intl } = this.props;

    return (
      <form className="communityForm">
        <FormDefaultComponent
          content={community.title}
          errorCodeMessage={valid_title ? valid_title.message : ''}
          id="title"
          isValid={valid_title ? !valid_title.isInvalid : undefined}
          onChange={this.handleChange.bind(this)}
          mandatory
          title={intl.formatMessage({ id: 'entity.info.title', defaultMessage: 'Title' })}
          placeholder={intl.formatMessage({
            id: 'community.form.title.placeholder',
            defaultMessage: 'An Awesome Group',
          })}
        />
        {/* {mode === "create" && */}
        <FormDefaultComponent
          content={community.short_title}
          errorCodeMessage={valid_short_title ? valid_short_title.message : ''}
          id="short_title"
          isValid={valid_short_title ? !valid_short_title.isInvalid : undefined}
          mandatory
          onChange={this.handleChange.bind(this)}
          pattern={/[A-Za-z0-9]/g}
          title={intl.formatMessage({ id: 'entity.info.short_name', defaultMessage: 'Short Name' })}
          prepend="#"
          placeholder={intl.formatMessage({
            id: 'community.form.short_title.placeholder',
            defaultMessage: 'AnAwesomeGroup',
          })}
        />
        {/* } */}
        <FormTextAreaComponent
          content={community.short_description}
          errorCodeMessage={valid_short_description ? valid_short_description.message : ''}
          id="short_description"
          isValid={valid_short_description ? !valid_short_description.isInvalid : undefined}
          mandatory
          maxChar={240}
          onChange={this.handleChange.bind(this)}
          title={intl.formatMessage({ id: 'entity.info.short_description', defaultMessage: 'Short description' })}
          rows={3}
          placeholder={intl.formatMessage({
            id: 'community.form.short_description.placeholder',
            defaultMessage: 'The group briefly explained',
          })}
        />
        {mode === 'edit' && (
          <FormWysiwygComponent
            id="description"
            title={intl.formatMessage({ id: 'entity.info.description', defaultMessage: 'Description' })}
            placeholder={intl.formatMessage({
              id: 'community.form.description.placeholder',
              defaultMessage: 'Describe your group in detail, with formatted text, images...',
            })}
            content={community.description}
            show
            onChange={this.handleChange.bind(this)}
          />
        )}
        {mode === 'edit' && (
          <FormImgComponent
            type="banner"
            id="banner_url"
            imageUrl={community.banner_url}
            itemId={community.id}
            itemType="communities"
            title={intl.formatMessage({ id: 'community.info.banner_url', defaultMessage: 'Group banner' })}
            content={community.banner_url}
            defaultImg="/images/default/default-group.jpg"
            onChange={this.handleChange.bind(this)}
          />
        )}
        <FormInterestsComponent
          content={community.interests}
          errorCodeMessage={valid_interests ? valid_interests.message : ''}
          id="interests"
          isValid={valid_interests ? !valid_interests.isInvalid : undefined}
          mandatory
          onChange={this.handleChange.bind(this)}
          title={intl.formatMessage({ id: 'entity.info.interests', defaultMessage: 'Interests' })}
        />
        <FormSkillsComponent
          content={community.skills}
          errorCodeMessage={valid_skills ? valid_skills.message : ''}
          id="skills"
          type="group"
          isValid={valid_skills ? !valid_skills.isInvalid : undefined}
          mandatory
          onChange={this.handleChange.bind(this)}
          title={intl.formatMessage({ id: 'entity.info.skills', defaultMessage: 'Skills' })}
          placeholder={intl.formatMessage({
            id: 'general.skills.placeholder',
            defaultMessage: 'Big data, Web Development, Open Science...',
          })}
        />
        <FormResourcesComponent
          content={community.ressources}
          id="ressources"
          type="group"
          mandatory={false}
          onChange={this.handleChange.bind(this)}
          title={intl.formatMessage({ id: 'entity.info.resources', defaultMessage: 'Resources' })}
          placeholder={intl.formatMessage({
            id: 'general.resources.placeholder',
            defaultMessage: '3D printers, Biolab, Makerspace...',
          })}
        />
        {mode === 'edit' && (
          <FormToggleComponent
            id="is_private"
            warningMsg="By choosing 'public', any member can join your group without your approval."
            warningMsgId="community.info.publicPrivateToggleMsg"
            title={intl.formatMessage({ id: 'entity.info.is_private', defaultMessage: 'Confidentiality' })}
            choice1={<FormattedMessage id="general.public" defaultMessage="Public" />}
            choice2={<FormattedMessage id="general.private" defaultMessage="Private" />}
            color1="#F9530B"
            color2="#27B40E"
            isChecked={community.is_private}
            onChange={this.handleChange.bind(this)}
          />
        )}
        {this.renderBtnsForm()}
      </form>
    );
  }
}
export default injectIntl(CommunityForm);
