/* eslint-disable @rushstack/no-null */
import { Component } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import { UserContext } from '~/contexts/UserProvider';
import NeedBtnStatus from './NeedBtnStatus';
import FormSkillsComponent from '~/components/Tools/Forms/FormSkillsComponent';
import FormResourcesComponent from '~/components/Tools/Forms/FormResourcesComponent';
/** * Validators ** */
import FormValidator from '~/components/Tools/Forms/FormValidator';
import needFormRules from './needFormRules.json';
import FormWysiwygComponent from '../Tools/Forms/FormWysiwygComponent';
import FormDefaultComponent from '../Tools/Forms/FormDefaultComponent';
// import "./NeedForm.scss";

class NeedForm extends Component {
  validator = new FormValidator(needFormRules);

  static get defaultProps() {
    return {
      action: 'create',
      cancel: () => console.warn('Missing cancel function'),
      handleChange: () => console.warn('Missing handleChange function'),
      handleSubmit: () => console.warn('Missing handleSubmit function'),
      need: undefined,
      uploading: false,
      refresh: () => console.warn('Missing refresh function'),
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      isChecked: this.props.need.is_urgent,
    };
  }

  handleChange(event, value) {
    const key = event;
    const content = value;

    /* Validators start */
    const state = {};
    state[key] = content;
    const validation = this.validator.validate(state);
    if (validation[key] !== undefined) {
      const stateValidation = {};
      stateValidation[`valid_${key}`] = validation[key];
      this.setState(stateValidation);
    }
    /* Validators end */
    this.props.handleChange(key, content);
    this.forceUpdate();
  }

  // handleChangeToggle(event) {
  //   this.setState({
  //     isChecked: !this.state.isChecked,
  //   });
  //   const key = event.target.id;
  //   const content = !this.state.isChecked;
  //   this.props.handleChange(key, content);
  // }

  handleSubmit(event) {
    event.preventDefault();
    /* Validators control before submit */
    const validation = this.validator.validate(this.props.need);
    if (validation.isValid) {
      this.props.handleSubmit();
    } else {
      const stateValidation = {};
      Object.keys(validation).forEach((key) => {
        if (key !== 'isValid') {
          stateValidation[`valid_${key}`] = validation[key];
        }
      });
      this.setState(stateValidation);
    }
  }

  renderInvalid(valid_obj) {
    if (valid_obj) {
      if (valid_obj.message !== '') {
        return (
          <div className="invalid-feedback">
            <FormattedMessage id={valid_obj.message} defaultMessage="Value is not valid" />
          </div>
        );
      }
    }
    // eslint-disable-next-line @rushstack/no-null
    return null;
  }

  render() {
    const { action, need, uploading } = this.props;
    const { formatMessage } = this.props.intl;
    const { valid_skills, valid_title, isChecked } = this.state ? this.state : '';
    const submitBtnText = action === 'create' ? 'Create' : 'Update';
    return (
      <div className="needForm">
        <div className="form-row">
          <div className="input-group col-12 col-md-6">
            <FormDefaultComponent
              id="title"
              content={need.title}
              title={formatMessage({ id: 'need.title', defaultMessage: 'Need Title' }) + '*'}
              placeholder={formatMessage({ id: 'need.title', defaultMessage: 'Need Title' })}
              onChange={this.handleChange.bind(this)}
            />
            {this.renderInvalid(valid_title)}
          </div>
          {/* <div className="input-group col-12 col-md-6 inputUrgent">
            <div className="isUrgent">
              <input
                type="checkbox"
                name="checkbox"
                id="is_urgent"
                checked={isChecked}
                onChange={this.handleChangeToggle.bind(this)}
              />
              <label htmlFor="is_urgent">
                <FormattedMessage id="need.isUrgent" defaultMessage="Is urgent?" />
              </label>
            </div>
          </div> */}

          <div className="input-group col-12 col-md-6">
            <FormDefaultComponent
              id="end_date"
              content={need.end_date && need.end_date.substr(0, 10)}
              title={formatMessage({ id: 'need.end_date', defaultMessage: 'Due on' })}
              onChange={this.handleChange.bind(this)}
              type="date"
              // set min date to to today
              minDate={new Date().toISOString().split('T')[0]}
            />
          </div>
          <div className="input-group col-12">
            <FormWysiwygComponent
              id="content"
              content={need.content}
              title=""
              placeholder={formatMessage({ id: 'need.content', defaultMessage: 'Detail your need' })}
              onChange={this.handleChange.bind(this)}
              show
            />
            {/* {this.renderInvalid(valid_content)} */}
          </div>
          <div className="input-group col-12">
            <FormattedMessage
              id="general.skills.placeholder"
              defaultMessage="Big data, Web Development, Open Science..."
            >
              {(skills) => (
                <FormSkillsComponent
                  className={`form-control ${
                    valid_skills ? (!valid_skills.isInvalid ? 'is-valid' : 'is-invalid') : ''
                  }`}
                  content={need.skills}
                  errorCodeMessage={valid_skills ? valid_skills.message : ''}
                  id="skills"
                  isValid={valid_skills ? !valid_skills.isInvalid : undefined}
                  onChange={this.handleChange.bind(this)}
                  placeholder={skills}
                  type="need"
                  title={<FormattedMessage id="need.skills.title" defaultMessage="Expected skills" />}
                />
              )}
            </FormattedMessage>
            {this.renderInvalid(valid_skills)}
          </div>
          <div className="input-group col-12">
            <FormattedMessage id="general.resources.placeholder" defaultMessage="3D printers, Biolab, Makerspace...">
              {(ressources) => (
                <FormResourcesComponent
                  className="form-control"
                  content={need.ressources}
                  id="ressources"
                  onChange={this.handleChange.bind(this)}
                  placeholder={ressources}
                  type="need"
                  title={<FormattedMessage id="need.resources.title" defaultMessage="Expected resources" />}
                />
              )}
            </FormattedMessage>
          </div>
        </div>
        <div className="actionBar">
          {this.context.isConnected ? need.is_owner ? <NeedBtnStatus need={need} /> : null : null}
          <button
            type="button"
            className="btn btn-outline-primary cancel"
            onClick={() => {
              this.props.cancel();
            }}
            disabled={uploading}
          >
            <FormattedMessage id="entity.form.btnCancel" defaultMessage="Cancel" />
          </button>
          <button
            className="btn btn-primary"
            disabled={!!uploading}
            onClick={this.handleSubmit.bind(this)}
            type="submit"
          >
            {uploading && (
              <>
                <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                &nbsp;
              </>
            )}
            <FormattedMessage id={`post.create.btn${submitBtnText}`} defaultMessage={submitBtnText} />
          </button>
        </div>
      </div>
    );
  }
}
NeedForm.contextType = UserContext;
export default injectIntl(NeedForm);
