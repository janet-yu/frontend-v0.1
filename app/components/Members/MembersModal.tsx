// eslint-disable-next-line no-unused-vars
import { useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useApi } from '~/contexts/apiContext';
import { useModal } from '~/contexts/modalContext';
import AsyncSelect from 'react-select/async';
import algoliasearch from 'algoliasearch';
import Box from '../Box';
import styled from '~/utils/styled';

export default function MembersModal({ itemType = '', itemId }) {
  const [emailInput, setEmailInput] = useState('');
  const [usersArray, setUsersArray] = useState([]);
  const [inviteSend, setInviteSend] = useState(false);
  const [error, setError] = useState(false);
  const api = useApi();
  const modal = useModal();
  const { formatMessage } = useIntl();
  const handleChangeEmail = (event) => {
    setEmailInput(event.target.value);
  };
  const resetState = () => {
    setEmailInput('');
    setInviteSend(false);
    setError(false);
  };

  const UserLabel = styled(Box)`
    align-items: center;
    div {
      font-size: 15px;
      color: color('gray-dark');
    }
    img {
      width: 28px;
      height: 28px;
      border-radius: 50%;
      object-fit: cover;
      margin-right: 6px;
    }
  `;

  const formatOptionLabel = ({ label, logo_url }) => (
    <UserLabel row>
      <img src={logo_url} />
      <div>{label}</div>
    </UserLabel>
  );

  const appId = process.env.ALGOLIA_APP_ID;
  const token = process.env.ALGOLIA_TOKEN;

  const client = algoliasearch(appId, token);
  const index = client.initIndex('User'); // User
  let algoliaMembers = [];

  const fetchAlgolia = (resolve, value) => {
    index
      .search(value, {
        attributesToRetrieve: ['id', 'nickname', 'first_name', 'last_name', 'logo_url_sm'],
        hitsPerPage: 5,
      })
      .then((content) => {
        if (content) {
          algoliaMembers = content.hits;
        } else {
          algoliaMembers = [''];
        }
        algoliaMembers = algoliaMembers.map((user) => {
          return { value: user.id, label: `${user.first_name} ${user.last_name}`, logo_url: user.logo_url_sm };
        });
        resolve(algoliaMembers);
      });
  };

  const loadOptions = (inputValue) =>
    new Promise((resolve) => {
      fetchAlgolia(resolve, inputValue);
    });

  const handleChangeUser = (content) => {
    const tempUsersArray = [];
    content &&
      content.map(function (user) {
        if (user) {
          tempUsersArray.push(user.value);
        }
      });
    setEmailInput('');
    setUsersArray(tempUsersArray);
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    if (itemType === 'projects' || itemType === 'communities' || itemType === 'challenges' || itemType === 'programs') {
      if (itemId) {
        let params = {};
        if (emailInput !== '') {
          // if user invited someone outside from JOGL
          params = { stranger_email: emailInput };
        } else {
          // if user invited someone from JOGL
          params = { user_ids: usersArray };
        }
        api
          .post(`/api/${itemType}/${itemId}/invite`, params)
          .then(() => {
            setInviteSend(true);
            setTimeout(() => {
              // close
              modal.setIsOpen(false);
              resetState();
              if (usersArray.length !== 0) {
                // show following alert message only if you invited a JOGL member
                alert(
                  usersArray.length === 1
                    ? // show different message if we added 1 or more users
                      formatMessage({
                        id: 'member.invite.added',
                        defaultMessage: 'Member was added! Please refresh the page to see the changes',
                      })
                    : formatMessage({
                        id: 'member.invite.added.plural',
                        defaultMessage: 'Members were added! Please refresh the page to see the changes',
                      })
                );
              }
            }, 1500);
          })
          .catch(() => {
            setError(true);
            setTimeout(() => {
              setError(false);
            }, 8000);
          });
      } else {
        console.warn('itemId is missing');
      }
    } else {
      console.warn('itemType not compatible');
    }
  };

  const customStyles = {
    container: (provided) => ({
      ...provided,
      width: '100%',
    }),
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="form-group">
        <label htmlFor="joglUser">
          <FormattedMessage id="member.invite.user.label" defaultMessage="JOGL user" />
        </label>
        <div className="input-group">
          <AsyncSelect
            isMulti
            cacheOptions
            styles={customStyles}
            onChange={handleChangeUser}
            formatOptionLabel={formatOptionLabel}
            placeholder={formatMessage({
              id: 'member.invite.user.placeholder',
              defaultMessage: 'Select user(s) to add',
            })}
            loadOptions={loadOptions}
            noOptionsMessage={() => null}
            components={{ DropdownIndicator: () => null, IndicatorSeparator: () => null }}
            isClearable
          />
        </div>
      </div>
      <Box className="text-center or" p={2}>
        <FormattedMessage id="member.invite.or" defaultMessage="Or" />
      </Box>
      <div className="form-group">
        <label htmlFor="byEmail">
          <FormattedMessage id="member.invite.mail.label" defaultMessage="User outside JOGL" />
        </label>
        <input
          type="email"
          className="form-control"
          id="byEmail"
          placeholder={formatMessage({
            id: 'member.invite.mail.placeholder',
            defaultMessage: 'Enter the email address of the person you want to invite.',
          })}
          value={emailInput}
          onChange={handleChangeEmail}
        />
      </div>
      <div className="text-center btnZone">
        <button
          type="submit"
          className="btn btn-primary btn-block"
          disabled={(emailInput === '' && usersArray.length === 0) || (emailInput && usersArray.length !== 0)}
        >
          {inviteSend ? (
            <FormattedMessage id="member.invite.btnSendEnded" defaultMessage="Invitation sent" />
          ) : (
            <FormattedMessage id="member.invite.btnSend" defaultMessage="Invite" />
          )}
        </button>
      </div>
      {error && (
        <div className="alert alert-danger" role="alert">
          <FormattedMessage id="err-" defaultMessage="An error has occurred" />
        </div>
      )}
    </form>
  );
}
