import { Component } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import Link from 'next/link';
import FormDefaultComponent from '~/components/Tools/Forms/FormDefaultComponent';
import FormImgComponent from '~/components/Tools/Forms/FormImgComponent';
import FormWysiwygComponent from '~/components/Tools/Forms/FormWysiwygComponent';
import FormDropdownComponent from '~/components/Tools/Forms/FormDropdownComponent';
import FormTextAreaComponent from '../Tools/Forms/FormTextAreaComponent';
import Box from '../Box';
// import "./ProgramForm.scss";

interface Props {
  mode: 'edit' | 'create';
  program: any;
  sending: boolean;
  handleChange: (key, content) => void;
  handleSubmit: () => void;
}

class ProgramForm extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      program: this.props.program,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  static get defaultProps() {
    return {
      mode: 'create',
      program: {
        id: 0,
        title: '',
        short_title: '',
        description: '',
        claps_count: 0,
        follower_count: 0,
        members_count: 0,
        launch_date: undefined,
        end_date: undefined,
      },
    };
  }

  handleChange(key, content) {
    this.props.handleChange(key, content);
  }

  handleSubmit() {
    this.props.handleSubmit(/* program */);
  }

  renderBtnsForm() {
    const { program, mode, sending } = this.props;
    let urlBack = { href: '/', as: '/' };
    let textAction = 'Create';
    if (mode === 'edit') {
      urlBack = { href: '/program/[short_title]', as: `/program/${program.short_title}` };
      textAction = 'Update';
    }
    return (
      <Box row justifyContent="center" marginTop={5}>
        <Link href={urlBack.href} as={urlBack.as}>
          <a>
            <button type="button" className="btn btn-outline-primary">
              <FormattedMessage id="entity.form.btnCancel" defaultMessage="Cancel" />
            </button>
          </a>
        </Link>
        <button
          type="button"
          onClick={this.handleSubmit}
          className="btn btn-primary"
          disabled={sending}
          style={{ marginRight: '10px' }}
        >
          {sending && (
            <>
              <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
              &nbsp;
            </>
          )}
          <FormattedMessage id={`entity.form.btn${textAction}`} defaultMessage={textAction} />
        </button>
      </Box>
    );
  }

  render() {
    const { program, intl } = this.props;
    return (
      <form className="programForm">
        <FormDefaultComponent
          id="title"
          content={program.title}
          title={intl.formatMessage({ id: 'entity.info.title', defaultMessage: 'Title' })}
          placeholder={intl.formatMessage({
            id: 'program.form.title.placeholder',
            defaultMessage: 'A Great Program',
          })}
          onChange={this.handleChange.bind(this)}
          mandatory
        />
        <FormDefaultComponent
          id="title_fr"
          content={program.title_fr}
          title={intl.formatMessage({ id: 'entity.info.title_fr', defaultMessage: 'Title fr' })}
          placeholder={intl.formatMessage({ id: 'program.form.title.placeholder', defaultMessage: 'A Great Program' })}
          onChange={this.handleChange.bind(this)}
        />
        <FormDefaultComponent
          id="short_title"
          content={program.short_title}
          title={intl.formatMessage({ id: 'entity.info.short_name', defaultMessage: 'Short Name' })}
          placeholder={intl.formatMessage({
            id: 'program.form.short_title.placeholder',
            defaultMessage: 'agreatprogram',
          })}
          onChange={this.handleChange.bind(this)}
          prepend="#"
          mandatory
          pattern={/[A-Za-z0-9]/g}
        />

        <FormTextAreaComponent
          content={program.short_description}
          id="short_description"
          maxChar={500}
          onChange={this.handleChange.bind(this)}
          rows={3}
          title={intl.formatMessage({ id: 'entity.info.short_description', defaultMessage: 'Short description' })}
          placeholder={intl.formatMessage({
            id: 'program.form.short_description.placeholder',
            defaultMessage: 'The program briefly explained',
          })}
          mandatory
        />
        <FormTextAreaComponent
          content={program.short_description_fr}
          id="short_description_fr"
          maxChar={500}
          onChange={this.handleChange.bind(this)}
          rows={3}
          title={intl.formatMessage({ id: 'entity.info.short_description_fr', defaultMessage: 'Short description FR' })}
          placeholder={intl.formatMessage({
            id: 'program.form.short_description.placeholder',
            defaultMessage: 'The program briefly explained',
          })}
        />
        <FormWysiwygComponent
          id="description"
          content={program.description}
          title={intl.formatMessage({ id: 'entity.info.description', defaultMessage: 'Description' })}
          placeholder={intl.formatMessage({
            id: 'program.form.description.placeholder',
            defaultMessage: 'Describe the program in detail, with formatted text, images...',
          })}
          onChange={this.handleChange.bind(this)}
          show
        />
        <FormWysiwygComponent
          id="description_fr"
          content={program.description_fr}
          title={intl.formatMessage({ id: 'entity.info.description_fr', defaultMessage: 'Description fr' })}
          placeholder={intl.formatMessage({
            id: 'program.form.description.placeholder',
            defaultMessage: 'Describe the program in detail, with formatted text, images...',
          })}
          onChange={this.handleChange.bind(this)}
        />
        <FormWysiwygComponent
          id="enablers"
          content={program.enablers}
          title={intl.formatMessage({ id: 'entity.info.enablers', defaultMessage: 'Enablers' })}
          placeholder={intl.formatMessage({
            id: 'program.form.meeting_information.placeholder',
            defaultMessage: 'Add the program sponsors and supporters (you can add link the link to the image)',
          })}
          onChange={this.handleChange.bind(this)}
        />
        <FormWysiwygComponent
          id="meeting_information"
          content={program.meeting_information}
          title={intl.formatMessage({ id: 'program.form.meeting_information', defaultMessage: 'Meeting information' })}
          placeholder={intl.formatMessage({
            id: 'program.form.meeting_information',
            defaultMessage: 'Meeting information',
          })}
          onChange={this.handleChange.bind(this)}
        />
        <FormImgComponent
          id="banner_url"
          content={program.banner_url}
          title={intl.formatMessage({ id: 'program.info.banner_url', defaultMessage: 'Program banner' })}
          imageUrl={program.banner_url}
          itemId={program.id}
          type="banner"
          itemType="programs"
          defaultImg="/images/default/default-program.jpg"
          onChange={this.handleChange.bind(this)}
          tooltipMessage={intl.formatMessage({
            id: 'challenge.info.banner_url.tooltip',
            defaultMessage:
              'For an optimal display, choose a visual in the format 2000 x 350 pixels (accepted formats: .png, .jpeg, .jpg; maximum weight: 2Mo).',
          })}
        />
        <FormImgComponent
          id="logo_url"
          content={program.logo_url}
          title={intl.formatMessage({ id: 'program.info.logo_url', defaultMessage: 'Program logo' })}
          imageUrl={program.logo_url}
          itemId={program.id}
          type="avatar"
          itemType="programs"
          defaultImg="/images/default/default-program.jpg"
          onChange={this.handleChange.bind(this)}
        />
        <FormDropdownComponent
          id="status"
          content={program.status}
          title={intl.formatMessage({ id: 'entity.info.status', defaultMessage: 'Status' })}
          options={['draft', 'soon', 'active', 'completed']}
          onChange={this.handleChange.bind(this)}
        />
        <FormDefaultComponent
          id="launch_date"
          content={program.launch_date ? program.launch_date.substr(0, 10) : undefined}
          title={intl.formatMessage({ id: 'entity.info.launch_date', defaultMessage: 'Launch date' })}
          onChange={this.handleChange.bind(this)}
          type="date"
        />
        <FormDefaultComponent
          id="end_date"
          content={program.end_date ? program.end_date.substr(0, 10) : undefined}
          title={intl.formatMessage({ id: 'entity.info.end_date', defaultMessage: 'End date' })}
          onChange={this.handleChange.bind(this)}
          type="date"
        />
        <FormDefaultComponent
          id="contact_email"
          content={program.contact_email}
          title={intl.formatMessage({ id: 'program.form.contact_email', defaultMessage: 'Contact email' })}
          placeholder={intl.formatMessage({
            id: 'program.form.contact_email.placeholder',
            defaultMessage: 'The program contact email',
          })}
          onChange={this.handleChange.bind(this)}
          mandatory
        />
        {this.renderBtnsForm()}
      </form>
    );
  }
}
export default injectIntl(ProgramForm);
