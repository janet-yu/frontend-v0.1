import styled from '~/utils/styled';
import Box from '../Box';
import { useTheme } from '~/utils/theme';

// set array of step numbers and colors depending on status value
export const statusToStep = (status) => {
  const theme = useTheme();
  switch (status) {
    case 'soon':
      return [0, theme.colors.greys['500']];
    case 'accepting':
      return [1, theme.colors.turquoise];
    case 'evaluating':
      return [2, theme.colors.darkBlue];
    case 'active':
      return [3, theme.colors.pink];
    case 'completed':
      return [4, theme.colors.violet];
    default:
      return undefined;
  }
};

export const StatusCircle = styled(Box)`
  background: #fff;
  height: 50px;
  width: 50px;
  border: 5px solid;
  border-radius: 50%;
  border-color: ${(p) =>
    p.step === 0
      ? p.theme.colors.greys['200']
      : p.step === 1
      ? `${p.theme.colors.turquoise} ${p.theme.colors.greys['200']} ${p.theme.colors.greys['200']}`
      : p.step === 2
      ? `${p.theme.colors.turquoise} ${p.theme.colors.darkBlue} ${p.theme.colors.greys['200']} ${p.theme.colors.greys['200']}`
      : p.step === 3
      ? `${p.theme.colors.turquoise} ${p.theme.colors.darkBlue} ${p.theme.colors.pink} ${p.theme.colors.greys['200']}`
      : p.step === 4 &&
        `${p.theme.colors.turquoise} ${p.theme.colors.darkBlue} ${p.theme.colors.pink} ${p.theme.colors.violet}`};
  transform: rotate(-135deg);
  > div {
    transform: rotate(135deg) translateY(-5px) translateX(13px);
    color: ${(p) => p.theme.colors.greys['500']};
  }
`;
